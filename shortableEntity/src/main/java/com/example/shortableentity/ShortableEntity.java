package com.example.shortableentity;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author norbert
 */
public abstract class ShortableEntity {

    /**
     * Get Identifiable object (URL).
     *
     * @return Integer with enumerated Id.
     */
    @XmlElement
    public Integer getId() {
        return null;
    }

    /**
     * Get class (for generic URL construction).
     *
     * @return String with class name.
     */
    @XmlElement
    public String getRelativePath() {
        return "dummy";
    }
}
