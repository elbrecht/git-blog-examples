package com.example.shortableentity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author norbert
 */
@Entity
@Table(name = "the_child")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TheChild.findAll", query = "SELECT t FROM TheChild t"),
    @NamedQuery(name = "TheChild.findById", query = "SELECT t FROM TheChild t WHERE t.id = :id"),
    @NamedQuery(name = "TheChild.findByPayload", query = "SELECT t FROM TheChild t WHERE t.payload = :payload")})
public class TheChild extends ShortableEntity implements Serializable {
    private static final long serialVersionUID = 3L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id", nullable = false)
    private Integer id;
    @Size(max = 1024)
    @Column(name = "payload", length = 1024)
    private String payload;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "child")
    private Collection<TheParent> theParentCollection;

    public TheChild() {
    }

    public TheChild(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @XmlTransient
    public Collection<TheParent> getTheParentCollection() {
        return theParentCollection;
    }

    public void setTheParentCollection(Collection<TheParent> theParentCollection) {
        this.theParentCollection = theParentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TheChild)) {
            return false;
        }
        TheChild other = (TheChild) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.shortableentity.TheChild[ id=" + id + " ]";
    }

}
