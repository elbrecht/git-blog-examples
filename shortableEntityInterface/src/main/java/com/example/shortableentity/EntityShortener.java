package com.example.shortableentity;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class EntityShortener extends XmlAdapter<ShortElement<ShortableEntity>, ShortableEntity> {

    private static final Logger LOG =
            Logger.getLogger(EntityShortener.class.getName());

    @Override
    public ShortableEntity unmarshal(final ShortElement<ShortableEntity> vt) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ShortElement<ShortableEntity> marshal(final ShortableEntity bt) throws Exception {
        LOG.log(Level.INFO, "EntityShortener adaptor, marshal: {0}", bt);
        if (bt == null) {
            return null;
        }
        return new ShortElement<ShortableEntity>(bt);
    }
}
