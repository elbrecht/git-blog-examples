package com.example.shortableentity;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author norbert
 */
public interface ShortableEntity {

    /**
     * Get Identifiable object (URL).
     *
     * @return Integer with enumerated Id.
     */
    @XmlElement
    public Integer getId();

    /**
     * Get class (for generic URL construction).
     *
     * @return String with class name.
     */
    @XmlElement
    public String getRelativePath();
}
