package com.example.shortableentity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author norbert
 */
@Entity
@Table(name = "the_parent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TheParent.findAll", query = "SELECT t FROM TheParent t"),
    @NamedQuery(name = "TheParent.findById", query = "SELECT t FROM TheParent t WHERE t.id = :id"),
    @NamedQuery(name = "TheParent.findByPayload", query = "SELECT t FROM TheParent t WHERE t.payload = :payload")})
public class TheParent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id", nullable = false)
    private Integer id;
    @Size(max = 1024)
    @Column(name = "payload", length = 1024)
    private String payload;
    @JoinColumn(name = "child", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private TheChild child;

    public TheParent() {
    }

    public TheParent(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @XmlJavaTypeAdapter(EntityShortener.class)
    public TheChild getChild() {
        return child;
    }

    public void setChild(TheChild child) {
        this.child = child;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TheParent)) {
            return false;
        }
        TheParent other = (TheParent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.shortableentity.TheParent[ id=" + id + " ]";
    }
}
