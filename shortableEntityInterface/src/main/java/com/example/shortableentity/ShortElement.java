package com.example.shortableentity;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @param <T>
 * @author norbert
 */
@XmlRootElement
public class ShortElement<T extends ShortableEntity> {

    /**
     * Id of object.
     */
    private Integer id;
    /**
     * Class identifier, so type of object.
     */
    private String relativePath = "someDefault";

    ShortElement() {
    }

    ShortElement(T entity) {
        this.id = entity.getId();
        this.relativePath = entity.getClass().getSimpleName().toLowerCase();
    }

    @XmlAttribute
    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    @XmlAttribute
    public String getRelativePath() {
        return this.relativePath;
    }

    public void setRelativePath(final String clazz) {
        this.relativePath = clazz;
    }
}
