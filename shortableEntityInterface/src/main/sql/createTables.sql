DROP TABLE the_parent;
DROP TABLE the_child;
CREATE TABLE the_child (
    id serial NOT NULL,
    payload character varying(1024),
    CONSTRAINT pk_the_child PRIMARY KEY (id )
);
CREATE TABLE the_parent (
    id serial NOT NULL,
    payload character varying(1024),
    child integer NOT NULL,
    CONSTRAINT pk_the_parent PRIMARY KEY (id ),
    CONSTRAINT fk_parent_child FOREIGN KEY (child)
        REFERENCES the_child (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);
